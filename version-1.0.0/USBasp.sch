EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:USBasp-cache
EELAYER 27 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 1 1
Title "USBasp"
Date "9 oct 2015"
Rev ""
Comp "Embedded Adda"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L USB JUSB1
U 1 1 5545DFD0
P 2400 1950
F 0 "JUSB1" H 2350 2350 60  0000 C CNN
F 1 "USB" V 2150 2100 60  0000 C CNN
F 2 "" H 2400 1950 60  0000 C CNN
F 3 "" H 2400 1950 60  0000 C CNN
	1    2400 1950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 5545DFE7
P 1700 2800
F 0 "#PWR01" H 1700 2800 30  0001 C CNN
F 1 "GND" H 1700 2730 30  0001 C CNN
F 2 "" H 1700 2800 60  0000 C CNN
F 3 "" H 1700 2800 60  0000 C CNN
	1    1700 2800
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR02
U 1 1 5545E02B
P 1700 1800
F 0 "#PWR02" H 1700 1890 20  0001 C CNN
F 1 "+5V" H 1700 1890 30  0000 C CNN
F 2 "" H 1700 1800 60  0000 C CNN
F 3 "" H 1700 1800 60  0000 C CNN
	1    1700 1800
	1    0    0    -1  
$EndComp
$Comp
L ATMEGA8-P IC1
U 1 1 5545E071
P 3450 6400
F 0 "IC1" H 2700 7700 40  0000 L BNN
F 1 "ATMEGA8-P" H 3950 4950 40  0000 L BNN
F 2 "DIL28" H 3450 6400 30  0000 C CIN
F 3 "" H 3450 6400 60  0000 C CNN
	1    3450 6400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5545E080
P 3450 8150
F 0 "#PWR03" H 3450 8150 30  0001 C CNN
F 1 "GND" H 3450 8080 30  0001 C CNN
F 2 "" H 3450 8150 60  0000 C CNN
F 3 "" H 3450 8150 60  0000 C CNN
	1    3450 8150
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR04
U 1 1 5545E15E
P 3450 4850
F 0 "#PWR04" H 3450 4940 20  0001 C CNN
F 1 "+5V" H 3450 4940 30  0000 C CNN
F 2 "" H 3450 4850 60  0000 C CNN
F 3 "" H 3450 4850 60  0000 C CNN
	1    3450 4850
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 5545E1D8
P 2100 5300
F 0 "R6" V 2180 5300 40  0000 C CNN
F 1 "10K" V 2107 5301 40  0000 C CNN
F 2 "~" V 2030 5300 30  0000 C CNN
F 3 "~" H 2100 5300 30  0000 C CNN
	1    2100 5300
	0    -1   -1   0   
$EndComp
$Comp
L +5V #PWR05
U 1 1 5545E216
P 1700 5150
F 0 "#PWR05" H 1700 5240 20  0001 C CNN
F 1 "+5V" H 1700 5240 30  0000 C CNN
F 2 "" H 1700 5150 60  0000 C CNN
F 3 "" H 1700 5150 60  0000 C CNN
	1    1700 5150
	1    0    0    -1  
$EndComp
$Comp
L CRYSTAL X1
U 1 1 5545E902
P 2050 6150
F 0 "X1" H 2050 6300 60  0000 C CNN
F 1 "CRYSTAL" H 2050 6000 60  0000 C CNN
F 2 "~" H 2050 6150 60  0000 C CNN
F 3 "~" H 2050 6150 60  0000 C CNN
	1    2050 6150
	0    -1   -1   0   
$EndComp
$Comp
L C C4
U 1 1 5545E9A5
P 1650 5850
F 0 "C4" H 1650 5950 40  0000 L CNN
F 1 "22pF" H 1656 5765 40  0000 L CNN
F 2 "~" H 1688 5700 30  0000 C CNN
F 3 "~" H 1650 5850 60  0000 C CNN
	1    1650 5850
	0    -1   -1   0   
$EndComp
$Comp
L C C5
U 1 1 5545E9B4
P 1650 6450
F 0 "C5" H 1650 6550 40  0000 L CNN
F 1 "22pF" H 1656 6365 40  0000 L CNN
F 2 "~" H 1688 6300 30  0000 C CNN
F 3 "~" H 1650 6450 60  0000 C CNN
	1    1650 6450
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR06
U 1 1 5545EA88
P 1200 6200
F 0 "#PWR06" H 1200 6200 30  0001 C CNN
F 1 "GND" H 1200 6130 30  0001 C CNN
F 2 "" H 1200 6200 60  0000 C CNN
F 3 "" H 1200 6200 60  0000 C CNN
	1    1200 6200
	1    0    0    -1  
$EndComp
$Comp
L LED D_LED1
U 1 1 5545EBD8
P 5000 5900
F 0 "D_LED1" H 5000 6000 50  0000 C CNN
F 1 "LED" H 5000 5800 50  0000 C CNN
F 2 "~" H 5000 5900 60  0000 C CNN
F 3 "~" H 5000 5900 60  0000 C CNN
	1    5000 5900
	-1   0    0    1   
$EndComp
$Comp
L LED D_LED2
U 1 1 5545EBE7
P 5000 6200
F 0 "D_LED2" H 5000 6300 50  0000 C CNN
F 1 "LED" H 5000 6100 50  0000 C CNN
F 2 "~" H 5000 6200 60  0000 C CNN
F 3 "~" H 5000 6200 60  0000 C CNN
	1    5000 6200
	-1   0    0    1   
$EndComp
$Comp
L R R4
U 1 1 5545ED55
P 5650 5900
F 0 "R4" V 5730 5900 40  0000 C CNN
F 1 "1K" V 5657 5901 40  0000 C CNN
F 2 "~" V 5580 5900 30  0000 C CNN
F 3 "~" H 5650 5900 30  0000 C CNN
	1    5650 5900
	0    -1   -1   0   
$EndComp
$Comp
L R R5
U 1 1 5545ED64
P 5650 6200
F 0 "R5" V 5730 6200 40  0000 C CNN
F 1 "1K" V 5657 6201 40  0000 C CNN
F 2 "~" V 5580 6200 30  0000 C CNN
F 3 "~" H 5650 6200 30  0000 C CNN
	1    5650 6200
	0    -1   -1   0   
$EndComp
$Comp
L +5V #PWR07
U 1 1 5545EE71
P 6100 5750
F 0 "#PWR07" H 6100 5840 20  0001 C CNN
F 1 "+5V" H 6100 5840 30  0000 C CNN
F 2 "" H 6100 5750 60  0000 C CNN
F 3 "" H 6100 5750 60  0000 C CNN
	1    6100 5750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 5545EF51
P 5050 6600
F 0 "#PWR08" H 5050 6600 30  0001 C CNN
F 1 "GND" H 5050 6530 30  0001 C CNN
F 2 "" H 5050 6600 60  0000 C CNN
F 3 "" H 5050 6600 60  0000 C CNN
	1    5050 6600
	1    0    0    -1  
$EndComp
Text Label 4800 6900 0    60   ~ 0
D+
Text Label 4900 5400 0    60   ~ 0
D+
Text Label 4900 5300 0    60   ~ 0
D-
$Comp
L CONN_6 P1
U 1 1 5545F374
P 6350 5200
F 0 "P1" V 6300 5200 60  0000 C CNN
F 1 "CONN_6" V 6400 5200 60  0000 C CNN
F 2 "" H 6350 5200 60  0000 C CNN
F 3 "" H 6350 5200 60  0000 C CNN
	1    6350 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 7900 3450 8150
Wire Wire Line
	3450 5000 3450 4850
Wire Wire Line
	2350 5300 2550 5300
Wire Wire Line
	1850 5300 1700 5300
Wire Wire Line
	1700 5300 1700 5150
Wire Wire Line
	1850 5850 2400 5850
Wire Wire Line
	2400 5850 2400 6000
Wire Wire Line
	2400 6000 2550 6000
Wire Wire Line
	1850 6450 2400 6450
Wire Wire Line
	2400 6450 2400 6200
Wire Wire Line
	2400 6200 2550 6200
Connection ~ 2050 6450
Connection ~ 2050 5850
Wire Wire Line
	1450 5850 1200 5850
Wire Wire Line
	1200 5850 1200 6200
Wire Wire Line
	1450 6450 1350 6450
Wire Wire Line
	1350 6450 1350 5850
Connection ~ 1350 5850
Wire Wire Line
	4800 5900 4650 5900
Wire Wire Line
	4650 5900 4650 6000
Wire Wire Line
	4650 6000 4450 6000
Wire Wire Line
	4800 6200 4650 6200
Wire Wire Line
	4650 6200 4650 6100
Wire Wire Line
	4650 6100 4450 6100
Wire Wire Line
	5400 6200 5200 6200
Wire Wire Line
	5200 5900 5400 5900
Wire Wire Line
	5900 5900 6100 5900
Wire Wire Line
	6100 5750 6100 6200
Wire Wire Line
	6100 6200 5900 6200
Connection ~ 6100 5900
Wire Wire Line
	4450 6200 4600 6200
Wire Wire Line
	4600 6200 4600 6300
Wire Wire Line
	4600 6300 4800 6300
Wire Wire Line
	4800 6300 4800 6400
Wire Wire Line
	4800 6400 5050 6400
Wire Wire Line
	5050 6400 5050 6600
Wire Wire Line
	4450 6900 4900 6900
Wire Wire Line
	4450 5400 5000 5400
Wire Wire Line
	4450 5300 5000 5300
Wire Wire Line
	4450 5500 5400 5500
Wire Wire Line
	5400 5500 5400 5150
Wire Wire Line
	5400 5150 6000 5150
Wire Wire Line
	4450 5600 5500 5600
Wire Wire Line
	5500 5600 5500 5250
Wire Wire Line
	5500 5250 6000 5250
Wire Wire Line
	4450 5700 4800 5700
Wire Wire Line
	4800 5700 4800 5650
Wire Wire Line
	4800 5650 5600 5650
Wire Wire Line
	5600 5650 5600 5350
Wire Wire Line
	5600 5350 6000 5350
Wire Wire Line
	4450 5800 4850 5800
Wire Wire Line
	4850 5800 4850 5700
Wire Wire Line
	4850 5700 5750 5700
Wire Wire Line
	5750 5700 5750 5450
Wire Wire Line
	5750 5450 6000 5450
$Comp
L +5V #PWR09
U 1 1 5545F52D
P 5800 5050
F 0 "#PWR09" H 5800 5140 20  0001 C CNN
F 1 "+5V" H 5800 5140 30  0000 C CNN
F 2 "" H 5800 5050 60  0000 C CNN
F 3 "" H 5800 5050 60  0000 C CNN
	1    5800 5050
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR010
U 1 1 5545F578
P 5750 4950
F 0 "#PWR010" H 5750 4950 30  0001 C CNN
F 1 "GND" H 5750 4880 30  0001 C CNN
F 2 "" H 5750 4950 60  0000 C CNN
F 3 "" H 5750 4950 60  0000 C CNN
	1    5750 4950
	0    1    1    0   
$EndComp
$Comp
L CP1 C1
U 1 1 5545F716
P 1350 2300
F 0 "C1" H 1400 2400 50  0000 L CNN
F 1 "4.7uF" H 1400 2200 50  0000 L CNN
F 2 "~" H 1350 2300 60  0000 C CNN
F 3 "~" H 1350 2300 60  0000 C CNN
	1    1350 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 2150 1700 2150
Wire Wire Line
	1700 2150 1700 1800
Wire Wire Line
	1350 2100 1350 1850
Wire Wire Line
	1350 1850 1700 1850
Connection ~ 1700 1850
Wire Wire Line
	1350 2500 1350 2700
Wire Wire Line
	1350 2700 1700 2700
Wire Wire Line
	1700 2300 1700 2800
Wire Wire Line
	2000 2300 1700 2300
Connection ~ 1700 2700
$Comp
L R R3
U 1 1 5545FB2D
P 2850 2550
F 0 "R3" V 2930 2550 40  0000 C CNN
F 1 "2.2K" V 2857 2551 40  0000 C CNN
F 2 "~" V 2780 2550 30  0000 C CNN
F 3 "~" H 2850 2550 30  0000 C CNN
	1    2850 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 2300 3350 2300
$Comp
L +5V #PWR011
U 1 1 5545FC3A
P 2850 2950
F 0 "#PWR011" H 2850 3040 20  0001 C CNN
F 1 "+5V" H 2850 3040 30  0000 C CNN
F 2 "" H 2850 2950 60  0000 C CNN
F 3 "" H 2850 2950 60  0000 C CNN
	1    2850 2950
	-1   0    0    1   
$EndComp
Wire Wire Line
	2850 2800 2850 2950
$Comp
L DIODESCH D2
U 1 1 5545FD4F
P 3150 2500
F 0 "D2" H 3150 2600 40  0000 C CNN
F 1 "3V6" H 3150 2400 40  0000 C CNN
F 2 "~" H 3150 2500 60  0000 C CNN
F 3 "~" H 3150 2500 60  0000 C CNN
	1    3150 2500
	0    -1   -1   0   
$EndComp
Connection ~ 2850 2300
$Comp
L GND #PWR012
U 1 1 5545FF26
P 3150 3000
F 0 "#PWR012" H 3150 3000 30  0001 C CNN
F 1 "GND" H 3150 2930 30  0001 C CNN
F 2 "" H 3150 3000 60  0000 C CNN
F 3 "" H 3150 3000 60  0000 C CNN
	1    3150 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 2700 3150 3000
$Comp
L R R1
U 1 1 55460039
P 3600 2300
F 0 "R1" V 3680 2300 40  0000 C CNN
F 1 "68" V 3607 2301 40  0000 C CNN
F 2 "~" V 3530 2300 30  0000 C CNN
F 3 "~" H 3600 2300 30  0000 C CNN
	1    3600 2300
	0    -1   -1   0   
$EndComp
Connection ~ 3150 2300
Wire Wire Line
	3850 2300 4250 2300
Text Label 4150 2300 0    60   ~ 0
D-
$Comp
L DIODESCH D1
U 1 1 55460260
P 2850 1950
F 0 "D1" H 2850 2050 40  0000 C CNN
F 1 "3V6" H 2850 1850 40  0000 C CNN
F 2 "~" H 2850 1950 60  0000 C CNN
F 3 "~" H 2850 1950 60  0000 C CNN
	1    2850 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	2750 2150 3350 2150
$Comp
L GND #PWR013
U 1 1 5546038A
P 2850 1600
F 0 "#PWR013" H 2850 1600 30  0001 C CNN
F 1 "GND" H 2850 1530 30  0001 C CNN
F 2 "" H 2850 1600 60  0000 C CNN
F 3 "" H 2850 1600 60  0000 C CNN
	1    2850 1600
	-1   0    0    1   
$EndComp
Wire Wire Line
	2850 1750 2850 1600
$Comp
L R R2
U 1 1 554604A1
P 3600 2150
F 0 "R2" V 3680 2150 40  0000 C CNN
F 1 "68" V 3607 2151 40  0000 C CNN
F 2 "~" V 3530 2150 30  0000 C CNN
F 3 "~" H 3600 2150 30  0000 C CNN
	1    3600 2150
	0    -1   -1   0   
$EndComp
Connection ~ 2850 2150
Wire Wire Line
	3850 2150 4250 2150
Text Label 4150 2150 0    60   ~ 0
D+
Wire Wire Line
	6000 5050 5800 5050
Wire Wire Line
	6000 4950 5750 4950
$EndSCHEMATC
