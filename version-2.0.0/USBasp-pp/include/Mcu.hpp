#ifndef MCU_HPP
#define MCU_HPP

#include <QString>
#include <QStringList>
#include <QMap>

class Mcu
{
    public:
        Mcu();
        ~Mcu();

        QString id,
                desc,
                signature,
                flash_size,
                eeprom_size;

        static void parse_config();
        static int default_label();
        static int totalMcusFound;
        static QMap<QString,Mcu> mcus_by_label;
        static QMap<QString,Mcu> mcus_by_id;
        static QStringList mcu_labels;

        Mcu & operator = (const Mcu &mcu);
};

#endif // MCU_HPP
